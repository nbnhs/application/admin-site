all: clean yarn lint script html

clean:
	@rm -rf public || mkdir public

build: script html

yarn:
	@yarn install --production=false

script: yarn
	@npx webpack-cli

html: yarn
	@cp -rf src/html/. public/
	@npx grunt

lint: yarn
	@npx tslint src/js/**/*.ts --fix

test: all
	( \
		cd public; \
		python3 -m http.server; \
	)
