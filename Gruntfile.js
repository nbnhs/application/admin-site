module.exports = function (grunt) {
  grunt.initConfig({
    assemble: {
      options: {
        marked: {
          breaks: false,
          gfm: true
        }
      },
      site: {
        src: "public/**/*.html",
        dest: "./"
      }
    }
  });

  grunt.loadNpmTasks("grunt-assemble");

  grunt.registerTask("default", ["assemble"]);
};
