import { SUPPORT_EMAIL } from "../../vars";
import {
  IApplication,
  getIncompleteApplications,
  getUnscoredApplications,
  getRejectedApplications,
  getPendingFurtherReviewApplications,
  getAcceptedApplications
} from "./applications";

function getAppsToView(): string {
  const query = window.location.search.substring(1);
  const vars = query.split("&");
  for (const variable of vars) {
    const pair = variable.split("=");
    if (decodeURIComponent(pair[0]) === "appstoview") {
      return decodeURIComponent(pair[1]);
    }
  }
  alert(`Malformed URL! Contact Milo @ ${SUPPORT_EMAIL} for tech support.`);

  return null;
}

window.onload = async () => {
  const toRetrieve = getAppsToView();
  if (toRetrieve !== null) {
    let retrievalFunction: () => Promise<IApplication[]>;
    if (toRetrieve === "incomplete") {
      retrievalFunction = getIncompleteApplications;
      document.getElementById("name-of-apps-list").innerHTML =
        "Incomplete Applications";
    } else if (toRetrieve === "unscored") {
      retrievalFunction = getUnscoredApplications;
      document.getElementById("name-of-apps-list").innerHTML =
        "Unscored Applications";
    } else if (toRetrieve === "accepted") {
      retrievalFunction = getAcceptedApplications;
      document.getElementById("name-of-apps-list").innerHTML =
        "Accepted Applications";
    } else if (toRetrieve === "rejected") {
      retrievalFunction = getRejectedApplications;
      document.getElementById("name-of-apps-list").innerHTML =
        "Rejected Applications";
    } else {
      retrievalFunction = getPendingFurtherReviewApplications;
      document.getElementById("name-of-apps-list").innerHTML =
        "Pending Further Review";
    }

    const result: IApplication[] = await retrievalFunction();
    const ul = document.createElement("ul");

    const anchor: HTMLButtonElement = document.createElement(
      "btn"
    ) as HTMLButtonElement;

    anchor.innerText = `Total: ${result.length}`;

    const elem = document.createElement("li");
    elem.appendChild(anchor);
    ul.appendChild(elem);

    document.getElementById("list-of-apps").append(ul);

    for (let i = 0; i < result.length; i++) {
      const anchor: HTMLButtonElement = document.createElement(
        "btn"
      ) as HTMLButtonElement;

      anchor.addEventListener("click", () => {
        document.cookie = `application=${JSON.stringify(result[i])}`;
        window.location.href = "view/index.html";
      });

      anchor.innerText = `${result[i].id}: ${result[i].first_name} ${result[i].last_name}`;

      const elem = document.createElement("li");
      elem.appendChild(anchor);
      ul.appendChild(elem);
    }
    document.getElementById("list-of-apps").append(ul);
  }
};
