import { RELAY_DOMAIN } from "../../vars";
import getCookie from "./view/getCookie";

function tallyPoints(points: string): number {
  const arr = points.split(",");
  let num = 0;
  for (const numAsString of arr) {
    const res = parseInt(numAsString, 10);

    if (isNaN(res)) {
      continue;
    }
    num += res;
  }

  return num;
}

export interface IApplication {
  first_name: string;
  last_name: string;
  id: number;
  leadership: string;
  service: string;
  signature: string;
  teacher: string;
  points: string;
}

export async function getUnscoredApplications(): Promise<IApplication[]> {
  const resp = await fetch(
    `https://${RELAY_DOMAIN}?operation=getCompleted&adminID=${getCookie(
      "admin"
    )}`
  );

  if (resp.ok) {
    console.log("Got completed applications.");
    const incomplete: IApplication[] = await resp.json();
    const unscored: IApplication[] = [];

    incomplete.forEach(application => {
      application.id = parseInt(application.leadership.split("/")[0], 10);
      if (!application.points) {
        application.points = "0,0,0,0";
        unscored.push(application);
      }
    });

    return unscored;
  } else {
    console.log("Failed to get unscored applications.");
  }
}
export async function getScoredApplications(): Promise<IApplication[]> {
  const resp = await fetch(
    `https://${RELAY_DOMAIN}?operation=getCompleted&adminID=${getCookie(
      "admin"
    )}`
  );

  if (resp.ok) {
    console.log("Got completed applications.");
    const incomplete: IApplication[] = await resp.json();
    const scored: IApplication[] = [];

    incomplete.forEach(application => {
      application.id = parseInt(application.leadership.split("/")[0], 10);
      if (application.points && !application.points.includes("-1")) {
        scored.push(application);
      }
    });

    return scored;
  } else {
    console.log("Failed to get scored applications.");
  }
}

export async function getAcceptedApplications(): Promise<IApplication[]> {
  const resp = await fetch(
    `https://${RELAY_DOMAIN}?operation=getCompleted&adminID=${getCookie(
      "admin"
    )}`
  );

  if (resp.ok) {
    console.log("Got completed applications.");
    const incomplete: IApplication[] = await resp.json();
    const acceptedApps: IApplication[] = [];

    incomplete.forEach(application => {
      application.id = parseInt(application.leadership.split("/")[0], 10);
      if (
        application.points &&
        !application.points.includes("-1") &&
        tallyPoints(application.points) >= 7
      ) {
        acceptedApps.push(application);
      }
    });

    return acceptedApps;
  } else {
    console.log("Failed to get accepted applications.");
  }
}

export async function getRejectedApplications(): Promise<IApplication[]> {
  const resp = await fetch(
    `https://${RELAY_DOMAIN}?operation=getCompleted&adminID=${getCookie(
      "admin"
    )}`
  );

  if (resp.ok) {
    console.log("Got completed applications.");
    const incomplete: IApplication[] = await resp.json();
    const rejectedApps: IApplication[] = [];

    incomplete.forEach(application => {
      application.id = parseInt(application.leadership.split("/")[0], 10);
      if (
        application.points &&
        !application.points.includes("-1") &&
        tallyPoints(application.points) < 7
      ) {
        rejectedApps.push(application);
      }
    });

    return rejectedApps;
  } else {
    console.log("Failed to get accepted applications.");
  }
}

export async function getPendingFurtherReviewApplications(): Promise<
  IApplication[]
> {
  const resp = await fetch(
    `https://${RELAY_DOMAIN}?operation=getCompleted&adminID=${getCookie(
      "admin"
    )}`
  );

  if (resp.ok) {
    console.log("Got completed applications.");
    const incomplete: IApplication[] = await resp.json();
    const pendingFurtherReview: IApplication[] = [];

    incomplete.forEach(application => {
      application.id = parseInt(application.leadership.split("/")[0], 10);
      if (application.points && application.points.includes("-1")) {
        pendingFurtherReview.push(application);
      }
    });

    return pendingFurtherReview;
  } else {
    console.log("Failed to get applications pending further review.");
  }
}

export async function getIncompleteApplications(): Promise<IApplication[]> {
  const resp = await fetch(
    `https://${RELAY_DOMAIN}?operation=getIncomplete&adminID=${getCookie(
      "admin"
    )}`
  );

  if (resp.ok) {
    console.log("Got incomplete applications.");
    const incomplete: IApplication[] = await resp.json();
    const result: IApplication[] = [];

    incomplete.forEach(application => {
      let appId = 0;

      for (const possibleContainer of [
        "leadership",
        "service",
        "signature",
        "teacher"
      ]) {
        if (application[possibleContainer] !== "Incomplete") {
          appId = parseInt(application[possibleContainer].split("/")[0], 10);
        }
      }

      application.id = appId;

      result.push(application);
    });

    return result;
  } else {
    console.log("Failed to get incomplete applications.");
  }
}
