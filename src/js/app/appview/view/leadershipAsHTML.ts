import { ILeadershipEntry } from "./interfaces";

export default function leadershipAsHTML(input: ILeadershipEntry[]): string {
    let res = "";
    let i = 1;
    input.forEach((entry) => {
        res += `<h2>Activity ${i}</h2>\n`;

        res += "<h3>Years Involved</h3>\n<p>";
        entry.yearsInvolved.forEach((year) => {
            res += year + " ";
        });
        res += "</p>\n";

        res += `<h3>Description</h3>\n<p>${entry.description}</p>`;

        i++;
    });

    return res;
}
