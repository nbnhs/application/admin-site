import { saveAs } from "file-saver";
import { RELAY_DOMAIN } from "../../../vars";
import blob2Base64 from "./blob2Base64";
import getCookie from "./getCookie";

export default async function downloadFromAWS(path: string, fileName: string) {
  const response = await getFromAWS(path);

  saveAs(await response.blob(), fileName);
}

export function getFromAWS(path: string): Promise<Response> {
  return fetch(
    `https://${RELAY_DOMAIN}?operation=getFromAWS&adminID=${getCookie(
      "admin",
    )}&path=${path}`,
  );
}

export async function getAWSImageAsBase64(path: string): Promise<string> {
  const blob = await (await getFromAWS(path)).blob();
  return await blob2Base64(blob);
}
