export interface ILeadershipEntry {
    yearsInvolved: string[];
    description: string;
}
