export default async function blob2Base64(blobData: Blob | Promise<Blob>): Promise<string> {
    const blob = await blobData;
    return new Promise((resolve, _) => {
        console.log("Converting blob to base64...");
        const reader = new FileReader();

        reader.onload = () => {
            console.log("Converted blob to base64!");
            resolve(String(reader.result));
        };

        reader.readAsDataURL(blob);
    });
}
