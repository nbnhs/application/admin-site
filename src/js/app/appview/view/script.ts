import { RELAY_DOMAIN, SUPPORT_EMAIL } from "../../../vars";
import { IApplication } from "../applications";
import { getAWSImageAsBase64, getFromAWS } from "./aws";
import getCookie from "./getCookie";
import { ILeadershipEntry } from "./interfaces";
import leadershipAsHTML from "./leadershipAsHTML";

const data: IApplication = JSON.parse(getCookie("application"));

window.onload = () => {
  document.getElementById(
    "name"
  ).innerText = `${data.first_name} ${data.last_name}`;
  document.getElementById("idnum").innerText = String(data.id);

  let values: string[];
  if (data.points === undefined || data.points === "Failed") {
    values = ["0", "0", "0", "0"];
  } else {
    values = data.points.split(",");
  }

  let i = 0;
  for (const subtotal of [
    "scholarship",
    "leadership",
    "service",
    "character"
  ]) {
    (document.getElementById(
      `${subtotal}-subtotal`
    ) as HTMLInputElement).value = values[i];
    i++;
  }

  for (const img of [data.service, data.teacher, data.signature]) {
    if (img === "Incomplete") {
      continue;
    }

    console.log(`Putting image at S3 path ${img} on site`);

    getAWSImageAsBase64(img).then(base64 => {
      console.log(`Got base64 for ${img}`);

      const imgElement = document.createElement("img") as HTMLImageElement;
      imgElement.setAttribute("src", base64);
      imgElement.setAttribute("class", "image");
      document.getElementById("images").appendChild(imgElement);

      console.log(`Done with ${img}`);
    });
  }

  getFromAWS(data.leadership).then(resp => {
    resp.json().then((entries: ILeadershipEntry[]) => {
      const divElement = document.createElement("div");
      divElement.setAttribute("class", "container");

      const aElement = document.createElement("a");
      aElement.innerHTML = leadershipAsHTML(entries);

      divElement.appendChild(aElement);
      document.body.appendChild(divElement);
    });
  });

  for (const item of ["scholarship", "leadership", "service", "character"]) {
    const checkbox = document.getElementById(
      `${item}-issue`,
    ) as HTMLInputElement;

    checkbox.onchange = _ => {
      (document.getElementById(`${item}-subtotal`) as HTMLInputElement).value =
        "-1";
    };
    if ((document.getElementById(`${item}-subtotal`) as HTMLInputElement).value === "-1") {
      checkbox.checked = true;
    }

  }

  const savePointsBtn = document.querySelector(
    ".save-points"
  ) as HTMLButtonElement;

  savePointsBtn.addEventListener("click", () => {
    savePointsBtn.innerHTML = "Please wait...";

    let totalPoints = "";
    for (const subtotal of [
      "scholarship",
      "leadership",
      "service",
      "character"
    ]) {
      totalPoints += `${
        (document.getElementById(`${subtotal}-subtotal`) as HTMLInputElement)
          .value
      },`;
    }

    const errMsg = `An error occured! Is your Internet connection up and running? If you continue to experience issues reach out to ${SUPPORT_EMAIL}.`;

    fetch(
      `https://${RELAY_DOMAIN}?operation=setPoints&adminID=${getCookie(
        "admin"
      )}&applicantID=${data.id}&points=${totalPoints}`
    )
      .then((val: Response) => {
        val
          .text()
          .then((resp: string) => {
            if (resp === "Success") {
              alert("Saved.");
              savePointsBtn.innerHTML = "Save Points";
            } else {
              alert(errMsg);
            }
          })
          .catch(_ => {
            alert(errMsg);
          });
      })
      .catch(_ => {
        alert(
          `An error occured! Is your Internet connection up and running? If you continue to experience issues reach out to ${SUPPORT_EMAIL}.`
        );
      });
  });
};
