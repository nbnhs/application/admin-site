window.onload = () => {
  ["incomplete", "unscored", "accepted", "rejected", "pending"].forEach((val) => {
    document.getElementById(`${val}-btn`).addEventListener("click", () => {
      window.location.href = `appview/index.html?appstoview=${val}`;
    });
  });
};
