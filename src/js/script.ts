import { RELAY_DOMAIN } from "./vars";

function getLoginToken(): string {
  const query = window.location.search.substring(1);
  const vars = query.split("&");
  for (const variable of vars) {
    const pair = variable.split("=");
    if (decodeURIComponent(pair[0]) === "token") {
      return decodeURIComponent(pair[1]);
    }
  }
  alert("Please use the URL you have been provided.");

  return null;
}

window.onload = () => {
  const login = getLoginToken();
  if (login != null) {

    console.log(`Got token ${login}`);

    const resp = fetch(
      `https://${RELAY_DOMAIN}?operation=authadmin&adminID=${login}`,
    );

    resp.then((val: Response) => {

      console.log("Got auth response");
      val.text().then((s: string) => {

        console.log(s);
        if (s === "Failure") {
          alert("That URL is invalid.");
        } else {
          document.cookie = `admin=${login}`;
          window.location.replace("app/index.html");
        }

      });
    });
  }
};
